module frez_control(
								clk,rst_n,frez_flag,
								datatx_high,
								datatx_low,
								datarx_high,
								datarx_low,
								tx_highbyte,
								tx_lowbyte,
								rx_highbyte,
								rx_lowbyte
								);
input clk;
input rst_n;
input frez_flag;
input [7:0]datatx_high;//MCU经Uart0发来的数据
input [7:0]datatx_low; //MCU经Uart0发来的数据
input [7:0]datarx_high;//MCU经Uart0发来的数据
input [7:0]datarx_low; //MCU经Uart0发来的数据

//parameter [7:0]datatx_high=8'h1B;//MCU经Uart0发来的数据
//parameter [7:0]datatx_low=8'h30; //MCU经Uart0发来的数据
//parameter [7:0]datarx_high=8'h02;//MCU经Uart0发来的数据
//parameter [7:0]datarx_low=8'h20; //MCU经Uart0发来的数据

output reg[7:0]tx_highbyte;//要发送或接收频率高字节
output reg[7:0]tx_lowbyte;//要发送或接收频率低字节
output reg[7:0]rx_highbyte;//要发送或接收频率高字节
output reg[7:0]rx_lowbyte;//要发送或接收频率低字节

always @(posedge clk or negedge rst_n)
	begin 
	if(!rst_n)//如果复位则发送初始值
		   begin 
			   tx_highbyte<=8'h20;
				tx_lowbyte<=8'hE8;//默发送频率为87.6MHz=(87 + 0.6*1)  学号301			
//			   tx_highbyte<=8'h21;
//				tx_lowbyte<=8'hE2;//默发送频率为100.1MHz
//				tx_highbyte<=8'h0F;
//				tx_lowbyte<=8'h50;//默认发送频率为93.1MHz
//				tx_highbyte<=8'h1B;
//				tx_lowbyte<=8'h90;//默认发送频率为93.1MHz
//				tx_highbyte<=8'h1C;
//				tx_lowbyte<=8'hD0;
//				tx_highbyte<=8'h24;
//				tx_lowbyte<=8'h50;
			end 
		else 
			case (frez_flag)//频率控制信号
				0:	//默认频率
					begin 
						tx_highbyte<=tx_highbyte;
						tx_lowbyte<=tx_lowbyte;
					end 
				1://判断频率
					begin 
					if(((datatx_high<8'h23)&&(datatx_high>8'h19))&&((datatx_low>=8'h00)&&(datatx_low<8'hff)))//87-108MHz
							begin 
								tx_highbyte<=datatx_high;
								tx_lowbyte<=datatx_low;
							end 
						else 
							begin 
								tx_highbyte<=8'h20;
								tx_lowbyte<=8'hE8;//默发送频率为87.6MHz=(87 + 0.6*1)MHz  学号301	
//								tx_highbyte<=8'h0F;
//								tx_lowbyte<=8'h50;//默认发送频率为93.1MHz
//								tx_highbyte<=8'h1C;
//								tx_lowbyte<=8'hD0;
//								tx_highbyte<=8'h24;
//								tx_lowbyte<=8'h50;
							end 
					end 
				default:;
			endcase
	end 
	
	always @(posedge clk or negedge rst_n)
	begin 
		if(!rst_n)
		   begin 
				rx_highbyte=8'h01;
				rx_lowbyte=8'h90;//设置接收频率86.7MHz  学号301
//				rx_highbyte=8'h33;
//				rx_lowbyte=8'h10;//设置接收频率107.4MHz=（108 - 0.6*1）  学号301
//				rx_highbyte<=8'h1C;
//				rx_lowbyte<=8'hD0;//设置默认接收频率为98.5MHZ
//				rx_highbyte<=8'h0f;
//				rx_lowbyte<=8'h50;//默认发送频率为93.1MHz
//				rx_highbyte<=8'h0A;
//				rx_lowbyte<=8'h10;//默认发送频率为93.1MHz
//				rx_highbyte=8'h02;
//				rx_lowbyte=8'h10;//03H写入0x01d0设置接收频率为87.8MHZ
//				rx_highbyte=8'h26;
//				rx_lowbyte=8'h90;
			end 
		else 
			case(frez_flag)
				0:
					begin 
						rx_highbyte<=rx_highbyte;
						rx_lowbyte<=rx_lowbyte;
					end 
				1:
					begin 
						if((datarx_high<8'h35)&&((datarx_low==8'h10)||(datarx_low==8'h50)||(datarx_low==8'h90)||(datarx_low==8'hd0)))
							begin 
								rx_highbyte<=datarx_high;
								rx_lowbyte<=datarx_low;
							end 
						else 
							begin
								rx_highbyte=8'h01;
		             		rx_lowbyte=8'h90;//设置接收频率86.7MHz  学号301
//								rx_highbyte=8'h33;
//								rx_lowbyte=8'h10;//设置接收频率107.4MHz=（108 - 0.6*1）  学号301					
//								rx_highbyte<=8'h1C;
//								rx_lowbyte<=8'hD0;//设置默认接收频率为98.5MHZ	
//								rx_highbyte<=8'h0f; 
//								rx_lowbyte<=8'h50;
//								rx_highbyte<=8'h20; 
//								rx_lowbyte<=8'hD0;
//								rx_highbyte=8'h02;
//								rx_lowbyte=8'h10;//03H写入0x01d0设置接收频率为87.8MHZ
//								rx_highbyte=8'h26;
//								rx_lowbyte=8'h90;
							end 
					end 
				default:;
			endcase
	end 
endmodule 