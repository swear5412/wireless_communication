/*修改：2015/7/24：在程序59行添加计数器，表示连续接收的位数*/
module bps_rx(clk,rst_n,rx_en,sel_data,num,recev_num,
					receive_flag);				/*波特率计数模块：1：确定发送数据与接收数据的有效范围
																					2: 进行分频计数：本程序使用波特率为9600bps，
																						则分频计数值为5207；*/

input clk;
input rst_n;
input rx_en;

output reg sel_data;
output reg[4:0]num; //计数从0到19,2帧数据
output reg[2:0]recev_num;//接收的帧数
output reg receive_flag;//未正确接收连续六个字节信号，计数记到一定值，recev_num==5且num==10值



parameter bps_div=13'd5207;//9600波特率分频计数值
parameter bsp_div_2=13'd2603;//计数一半时采样

parameter total_cnt=32'd24999999;//计数值
parameter total_cnt_max=32'd25999999;//计数值
//parameter total_cnt=32'd205590;//计数值
//parameter total_cnt_max=32'd205800;//计数值
//parameter total_cnt=32'd208280;//计数值
//parameter total_cnt=32'd208281;//计数值
//parameter total_cnt=32'd208282;//计数值
//parameter total_cnt=32'd208279;//计数值
//parameter total_cnt=32'd208278;//计数值


reg flag;  //表示发送/接收标志位：
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   flag<=0;
	else 
	   if(rx_en) //当接收到使能信号后将flag标志位拉高
		   flag<=1;
		else 
		   if(num==5'd10)
			   flag<=0;
			else 
			   flag<=flag;
//wire flag;
//assign flag=(rx_en)?1'b1:((num==5'd10)?1'b0:flag);

reg in_c;
reg in_d;
wire total_num_en;

wire receive_clear_flag;//若检测到receive_clear_flag信号，则让receive_flag==1；若检测到接收信号，则为1；


always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   begin
		   in_c<=1'b0; 
			in_d<=1'b0;
		end 
	else 
	   begin 
		   in_c<=flag;
			in_d<=in_c;
		end

assign total_num_en=(recev_num==0)?in_c&(~in_d):1'b0;		

//reg total_num_flag;
//
//always @(posedge clk or negedge rst_n)
//	if(!rst_n)
//		total_num_flag<=1'b0;
//	else
//		if((total_num_en)&&(!receive_clear_flag))
//			total_num_flag<=1'b1;
//		else 
//			if((num==10)&&(recev_num==3)||(receive_clear_flag))
//				total_num_flag<=0;
				
reg total_num_flag;

always @(posedge clk or negedge rst_n)
	if(!rst_n)
		total_num_flag<=1'b0;
	else
		if(total_num_en)
			total_num_flag<=1'b1;
		else 
				total_num_flag<=total_num_flag;


//reg receive_flag;//若检测到receive_clear_flag信号，则让receive_flag==1；若检测到接收信号，则为1；
always @(posedge clk or negedge rst_n)
	if(!rst_n)
		receive_flag<=1'b0;
	else
		if(receive_clear_flag)
			receive_flag<=1'b1;
		else 
			if(rx_en)
				receive_flag<=1'b0;
			else
				receive_flag<=receive_flag;
reg [31:0]rx_total_num;		//连续接收到的bit数		
always @(posedge clk or negedge rst_n)
	if(!rst_n)
	   rx_total_num<=1'd0;
	else
		if((total_num_en)||(rx_total_num>total_cnt_max))
			rx_total_num<=1'd0;
		else
			rx_total_num<=rx_total_num+1'b1;
			
reg [12:0]cnt;
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   cnt<=13'd0;
	else 
	   if(flag&&cnt<bps_div)
		   cnt<=cnt+1'b1;
		else 
		   cnt<=13'd0;
			
/*always @(posedge clk or negedge rst_n)
	if(!rst_n)
	   num<=5'd0;
	else 
	   if(sel_data&&flag)
		   num<=num+1'b1;
		else 
		   if(num==5'd10)
			   num<=1'd0;*/
always @(posedge clk or negedge rst_n)
	if(!rst_n)
	   num<=5'd0;
	else
		if(num==5'd10)	
			num<=1'd0;
		else 
			if(sel_data&&flag)
				num<=num+1'b1;
				
reg [5:0]bit_num;
always @(posedge clk or negedge rst_n)
	if(!rst_n)
	   bit_num<=6'd0;
	else
		if(rx_total_num==total_cnt_max)
			bit_num<=6'd0;
			else
				if(sel_data&&flag)
					bit_num<=bit_num+1'b1;
		
always @(posedge clk or negedge rst_n)
	if(!rst_n)
		begin
			recev_num<=3'd0;
		end
	else 
		if(receive_clear_flag)
			recev_num<=3'd0;
		else
			if(num==10)
				recev_num<=recev_num+1'b1;
			else
				recev_num<=recev_num;

	reg [3:0]Byte_num;								
always @(posedge clk or negedge rst_n)
	if(!rst_n)
		begin
			Byte_num<=4'd0;
		end
	else 
		if(rx_total_num==total_cnt_max)
			Byte_num<=4'd0;
		else
			if(num==10)
				begin
					Byte_num<=Byte_num+1'b1;
				end
			else
				Byte_num<=Byte_num;

assign  receive_clear_flag=((rx_total_num==total_cnt)&&(Byte_num!==6)&&(bit_num!==60))?1'b1:1'b0;
//reg recev_num_1;
//always @(posedge clk or negedge rst_n)	
//		if(!rsr_n)
//			recev_num_1<=0;
//		else
//			if(!uart_rst)
//				recev_num_1<=recev_num;
//			else
//				recev_num_1<=1'b0;*/
//			
//wire uart_rst;
//assign uart_rst=((rx_total_num==total_cnt)&&(recev_num==3))?1'b0:1'b1;	
//
//reg [1:0]recev_num_1;
//always @(posedge clk or negedge rst_n)
//	if(!rst_n)
//	   recev_num_1<=2'd0;
//	else 
//		   if(num==10)
//				recev_num_1<=recev_num_1+1'b1;
//			else 
//				recev_num_1<=recev_num_1;

//always @(posedge clk or negedge rst_n)	
//		if(!rst_n)
//			recev_num<=0;
//		else
//			if(!uart_rst)
//				recev_num<=recev_num_1;
//			else
//				recev_num<=1'b0;		
				
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   sel_data<=1'b0;
	else 
	   if(cnt==bsp_div_2)
		   sel_data<=1'b1;
		else
		   sel_data<=1'b0;
endmodule 
		
				