module freq_400k_re(clk,rst_n,freq_out_re);
input clk;
input rst_n;

output reg freq_out_re;

reg [7:0]cnt;

always @(posedge clk or negedge rst_n)
begin 
   if(!rst_n)
	   begin 
	      cnt<=0;
			freq_out_re<=0;
		end 
   else 
	   begin 
		   cnt<=cnt+1'd1;
			   if(cnt==62)
				   begin 
				      freq_out_re<=~freq_out_re;
						cnt<=0;
					end 
				else 
				   freq_out_re<=freq_out_re;
		end 
	   
end 
endmodule 