module freq_400k_tx(clk,rst_n,freq_out_tx);
input clk;
input rst_n;

output reg freq_out_tx;

reg [7:0]cnt;

always @(posedge clk or negedge rst_n)
begin 
   if(!rst_n)
	   begin 
	      cnt<=0;
			freq_out_tx<=0;
		end 
   else 
	   begin 
		   cnt<=cnt+1'd1;
			   if(cnt==62)
				   begin 
				      freq_out_tx<=~freq_out_tx;
						cnt<=0;
					end 
				else 
				   freq_out_tx<=freq_out_tx;
		end 
	   
end 
endmodule 