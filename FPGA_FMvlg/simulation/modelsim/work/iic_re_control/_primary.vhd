library verilog;
use verilog.vl_types.all;
entity iic_re_control is
    generic(
        chip_id         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi1, Hi0, Hi0, Hi0, Hi1, Hi0);
        addr_02         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi0);
        data_high_rst_02: vl_logic_vector(0 to 7) := (Hi1, Hi1, Hi0, Hi1, Hi0, Hi0, Hi0, Hi0);
        data_low_rst_02 : vl_logic_vector(0 to 7) := (Hi1, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi1);
        data_high_pow_02: vl_logic_vector(0 to 7) := (Hi1, Hi1, Hi0, Hi1, Hi0, Hi0, Hi0, Hi0);
        data_low_pow_02 : vl_logic_vector(0 to 7) := (Hi1, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1);
        addr_05         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi0, Hi1);
        data_voice_high : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        data_voice_low  : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi1, Hi1, Hi1, Hi1, Hi1, Hi1);
        addr_40         : vl_logic_vector(0 to 7) := (Hi0, Hi1, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        data_tr_high    : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        data_tr_low     : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        addr_03         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi1);
        delay_cnt       : integer := 10000;
        delay_cnt1      : integer := 100000;
        delay_powerup   : integer := 1500000
    );
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        data_frez_high  : in     vl_logic_vector(7 downto 0);
        data_frez_low   : in     vl_logic_vector(7 downto 0);
        SCLK_RE         : in     vl_logic;
        SDA_RE          : inout  vl_logic;
        flag_re         : out    vl_logic;
        iic_en          : in     vl_logic;
        receive_flag    : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of chip_id : constant is 1;
    attribute mti_svvh_generic_type of addr_02 : constant is 1;
    attribute mti_svvh_generic_type of data_high_rst_02 : constant is 1;
    attribute mti_svvh_generic_type of data_low_rst_02 : constant is 1;
    attribute mti_svvh_generic_type of data_high_pow_02 : constant is 1;
    attribute mti_svvh_generic_type of data_low_pow_02 : constant is 1;
    attribute mti_svvh_generic_type of addr_05 : constant is 1;
    attribute mti_svvh_generic_type of data_voice_high : constant is 1;
    attribute mti_svvh_generic_type of data_voice_low : constant is 1;
    attribute mti_svvh_generic_type of addr_40 : constant is 1;
    attribute mti_svvh_generic_type of data_tr_high : constant is 1;
    attribute mti_svvh_generic_type of data_tr_low : constant is 1;
    attribute mti_svvh_generic_type of addr_03 : constant is 1;
    attribute mti_svvh_generic_type of delay_cnt : constant is 1;
    attribute mti_svvh_generic_type of delay_cnt1 : constant is 1;
    attribute mti_svvh_generic_type of delay_powerup : constant is 1;
end iic_re_control;
