transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/uart_tx.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/uart_rx.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/UART_IIC_TR.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/SCL_TX.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/SCL_RE.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/iic_tx_control.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/iic_re_control.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/frez_control.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/freq_400k_tx.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/freq_400k_re.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/bps_tx.v}
vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/bps_rx.v}

vlog -vlog01compat -work work +incdir+D:/about\ FPGA/FPGA_FMvlg {D:/about FPGA/FPGA_FMvlg/tb.v}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cycloneive_ver -L rtl_work -L work -voptargs="+acc"  tb

add wave *
view structure
view signals
run -all
