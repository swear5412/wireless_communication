library verilog;
use verilog.vl_types.all;
entity bps_tx is
    generic(
        bps_div         : vl_logic_vector(0 to 12) := (Hi1, Hi0, Hi1, Hi0, Hi0, Hi0, Hi1, Hi0, Hi1, Hi0, Hi1, Hi1, Hi1);
        bsp_div_2       : vl_logic_vector(0 to 12) := (Hi0, Hi1, Hi0, Hi1, Hi0, Hi0, Hi0, Hi1, Hi0, Hi1, Hi0, Hi1, Hi1)
    );
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        en              : in     vl_logic;
        sel_data1       : out    vl_logic;
        num1            : out    vl_logic_vector(4 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of bps_div : constant is 1;
    attribute mti_svvh_generic_type of bsp_div_2 : constant is 1;
end bps_tx;
