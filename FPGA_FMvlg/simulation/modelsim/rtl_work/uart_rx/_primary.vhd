library verilog;
use verilog.vl_types.all;
entity uart_rx is
    generic(
        delay           : integer := 24999999
    );
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        fpga_rx         : in     vl_logic;
        num             : in     vl_logic_vector(4 downto 0);
        recev_num       : in     vl_logic_vector(1 downto 0);
        sel_data        : in     vl_logic;
        rx_en           : out    vl_logic;
        uart_en         : in     vl_logic;
        tx_en           : out    vl_logic;
        rx_data         : out    vl_logic_vector(7 downto 0);
        frez_flag       : out    vl_logic;
        data_tx_high    : out    vl_logic_vector(7 downto 0);
        data_tx_low     : out    vl_logic_vector(7 downto 0);
        data_rx_high    : out    vl_logic_vector(7 downto 0);
        data_rx_low     : out    vl_logic_vector(7 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of delay : constant is 1;
end uart_rx;
