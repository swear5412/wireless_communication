library verilog;
use verilog.vl_types.all;
entity freq_400k_re is
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        freq_out_re     : out    vl_logic
    );
end freq_400k_re;
