library verilog;
use verilog.vl_types.all;
entity iic_tx_control is
    generic(
        chip_id         : vl_logic_vector(0 to 7) := (Hi0, Hi1, Hi0, Hi1, Hi1, Hi0, Hi0, Hi0);
        addr_00         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0);
        data_rst_00     : vl_logic_vector(0 to 7) := (Hi1, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1);
        data_cal_00     : vl_logic_vector(0 to 7) := (Hi0, Hi1, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1);
        data_calclp_00  : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1);
        addr_03         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi1);
        data_extc_03    : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi1, Hi0, Hi0, Hi0, Hi0);
        addr_04         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi0, Hi0);
        data_chsclk_04  : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi1, Hi1, Hi0, Hi0, Hi1, Hi1);
        addr_18         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi1, Hi1, Hi0, Hi0, Hi0);
        data_snr_18     : vl_logic_vector(0 to 7) := (Hi1, Hi1, Hi1, Hi0, Hi0, Hi1, Hi0, Hi0);
        addr_1B         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi1, Hi1, Hi0, Hi1, Hi1);
        data_rfmax_1B   : vl_logic_vector(0 to 7) := (Hi1, Hi1, Hi1, Hi1, Hi0, Hi0, Hi0, Hi0);
        addr_02         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1, Hi0);
        data_pacls_02   : vl_logic_vector(0 to 7) := (Hi1, Hi0, Hi1, Hi1, Hi1, Hi0, Hi0, Hi1);
        addr_01         : vl_logic_vector(0 to 7) := (Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi0, Hi1);
        delay_cnt       : integer := 20000;
        delay_powerup   : integer := 1500000
    );
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        data_frez_high  : in     vl_logic_vector(7 downto 0);
        data_frez_low   : in     vl_logic_vector(7 downto 0);
        SCLK_TX         : in     vl_logic;
        SDA_TX          : inout  vl_logic;
        flag_tx         : out    vl_logic;
        uart_en         : out    vl_logic;
        frez_flag       : in     vl_logic;
        receive_flag    : in     vl_logic;
        iic_en          : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of chip_id : constant is 1;
    attribute mti_svvh_generic_type of addr_00 : constant is 1;
    attribute mti_svvh_generic_type of data_rst_00 : constant is 1;
    attribute mti_svvh_generic_type of data_cal_00 : constant is 1;
    attribute mti_svvh_generic_type of data_calclp_00 : constant is 1;
    attribute mti_svvh_generic_type of addr_03 : constant is 1;
    attribute mti_svvh_generic_type of data_extc_03 : constant is 1;
    attribute mti_svvh_generic_type of addr_04 : constant is 1;
    attribute mti_svvh_generic_type of data_chsclk_04 : constant is 1;
    attribute mti_svvh_generic_type of addr_18 : constant is 1;
    attribute mti_svvh_generic_type of data_snr_18 : constant is 1;
    attribute mti_svvh_generic_type of addr_1B : constant is 1;
    attribute mti_svvh_generic_type of data_rfmax_1B : constant is 1;
    attribute mti_svvh_generic_type of addr_02 : constant is 1;
    attribute mti_svvh_generic_type of data_pacls_02 : constant is 1;
    attribute mti_svvh_generic_type of addr_01 : constant is 1;
    attribute mti_svvh_generic_type of delay_cnt : constant is 1;
    attribute mti_svvh_generic_type of delay_powerup : constant is 1;
end iic_tx_control;
