library verilog;
use verilog.vl_types.all;
entity UART_IIC_TR is
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        fpga_rx         : in     vl_logic;
        fpga_tx         : out    vl_logic;
        SDA_RE          : inout  vl_logic;
        SDA_TX          : inout  vl_logic;
        SCLK_RE         : out    vl_logic;
        SCLK_TX         : out    vl_logic
    );
end UART_IIC_TR;
