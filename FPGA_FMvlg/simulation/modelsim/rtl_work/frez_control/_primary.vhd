library verilog;
use verilog.vl_types.all;
entity frez_control is
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        frez_flag       : in     vl_logic;
        datatx_high     : in     vl_logic_vector(7 downto 0);
        datatx_low      : in     vl_logic_vector(7 downto 0);
        datarx_high     : in     vl_logic_vector(7 downto 0);
        datarx_low      : in     vl_logic_vector(7 downto 0);
        tx_highbyte     : out    vl_logic_vector(7 downto 0);
        tx_lowbyte      : out    vl_logic_vector(7 downto 0);
        rx_highbyte     : out    vl_logic_vector(7 downto 0);
        rx_lowbyte      : out    vl_logic_vector(7 downto 0)
    );
end frez_control;
