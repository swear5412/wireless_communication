library verilog;
use verilog.vl_types.all;
entity SCL_RE is
    generic(
        delay           : integer := 1499999
    );
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        flag_re         : in     vl_logic;
        SCLK_RE         : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of delay : constant is 1;
end SCL_RE;
