library verilog;
use verilog.vl_types.all;
entity uart_tx is
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        num             : in     vl_logic_vector(4 downto 0);
        sel_data        : in     vl_logic;
        rx_data         : in     vl_logic_vector(7 downto 0);
        fpga_tx         : out    vl_logic
    );
end uart_tx;
