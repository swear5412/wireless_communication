library verilog;
use verilog.vl_types.all;
entity bps_rx is
    generic(
        bps_div         : vl_logic_vector(0 to 12) := (Hi1, Hi0, Hi1, Hi0, Hi0, Hi0, Hi1, Hi0, Hi1, Hi0, Hi1, Hi1, Hi1);
        bsp_div_2       : vl_logic_vector(0 to 12) := (Hi0, Hi1, Hi0, Hi1, Hi0, Hi0, Hi0, Hi1, Hi0, Hi1, Hi0, Hi1, Hi1);
        total_cnt       : integer := 24999999;
        total_cnt_max   : integer := 25999999
    );
    port(
        clk             : in     vl_logic;
        rst_n           : in     vl_logic;
        rx_en           : in     vl_logic;
        sel_data        : out    vl_logic;
        num             : out    vl_logic_vector(4 downto 0);
        recev_num       : out    vl_logic_vector(1 downto 0);
        receive_flag    : out    vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of bps_div : constant is 1;
    attribute mti_svvh_generic_type of bsp_div_2 : constant is 1;
    attribute mti_svvh_generic_type of total_cnt : constant is 1;
    attribute mti_svvh_generic_type of total_cnt_max : constant is 1;
end bps_rx;
