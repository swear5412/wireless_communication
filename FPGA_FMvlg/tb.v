//`timescale 1ns/1ps//仿真时间单位/时间精度
`timescale 1ps/1ps//仿真时间单位/时间精度
module tb;
reg clk;
reg rst_n;

reg fpga_rx;
wire fpga_tx;
wire SDA_RE;
wire SDA_TX; 

wire SCLK_RE;
wire SCLK_TX;

initial
	begin 
		clk=0;
		rst_n=0;
		fpga_rx=1;
//		#20.1
      #20
		rst_n=1;
		#5500000 fpga_rx=0;
		#104166  fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		
		
		#2200000 fpga_rx=0;
		#104166  fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		
		#114066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		
		
		#3300000  fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		
		#2200000 fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		
		
		#2200000  fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		
		#3300000 fpga_rx=0;
		#104166  fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=0;
		#104066	fpga_rx=1;
		
	end 
 
always #10 clk = ~clk;


UART_IIC_TR UART_IIC_TR(
									.clk(clk),
									.rst_n(rst_n),
									.fpga_rx(fpga_rx),
									.fpga_tx(fpga_tx),
									.SDA_RE(SDA_RE),
									.SDA_TX(SDA_TX),
									.SCLK_RE(SCLK_RE),
									.SCLK_TX(SCLK_TX)
									);
endmodule 