//串口接收模块：1：确认开始位；0：确认停止位
//修改：2015/7/24，于程序96—112处添加了frez_flag控制信号，控制RDA5820的03H寄存器的值，即控制频率
module uart_rx(clk,rst_n,fpga_rx,num,recev_num,sel_data,rx_en,uart_en,
							tx_en,rx_data,frez_flag,
							data_tx_high,data_tx_low,data_rx_high,data_rx_low
					);
input clk;
input rst_n;
input uart_en;
input fpga_rx;  //输入串行数据
input sel_data; //波特率计数的中心点（采集数据的使能信号）
input [4:0]num; //帧数据控制位
input [2:0]recev_num;//接收帧数

//input receive_clear_flag;

//output frez_en;//将此信号作为iic启动信号
output rx_en;   //使能信号：启动接收波特率计数
output reg tx_en;   //使能信号：接收完数据后，开始启动发送数据模块
output reg frez_flag;//发送/接收频率控制信号,并作为串口发送完毕后IIC的启动信号
output reg [7:0]data_tx_high; //接收到的8位有效串行数据转换为8位并行数据
output reg [7:0]data_tx_low;
output reg [7:0]data_rx_high; //接收到的8位有效串行数据转换为8位并行数据
output reg [7:0]data_rx_low;

output reg[7:0]rx_data;

//output wrong_data_flag;

reg[7:0]rx_data_buff;
reg[7:0]data_tx_start; //接收到的8位数据枕头
reg[7:0]data_tx_end; //接收到的8位数据帧尾
reg[7:0]data_tx_high_buff; //接收到的8位有效串行数据转换为8位并行数据
reg[7:0]data_tx_low_buff;
reg[7:0]data_rx_high_buff; //接收到的8位有效串行数据转换为8位并行数据
reg[7:0]data_rx_low_buff;

reg [31:0]delay_cnt;

parameter delay=32'd24999999;
//parameter delay=32'd10;

always @(posedge clk or negedge rst_n)
	if(!rst_n)
		delay_cnt<=1'd0;
	else
		if(!uart_en)
			delay_cnt<=1'd0;
		else
			if(delay_cnt<delay)
				delay_cnt<=delay_cnt+1'd1;
			else
				delay_cnt<=delay;
reg in_1;
reg in_2;
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   begin
		   in_1<=1'b1;
			in_2<=1'b1;
		end 
	else 
	   begin 
		   in_1<=fpga_rx;
			in_2<=in_1;
		end 
		
assign rx_en=((uart_en)&&(delay_cnt==delay))?in_2&(~in_1):1'b0;//当检测到由高变低时，将使能信号拉高，启动接收

always @(posedge clk or negedge rst_n)
	if(!rst_n)//如果系统复位则开始给寄存器赋值
      begin 
		   rx_data_buff<=8'd0;//接收数据缓存器赋0
			data_tx_start<=8'd0;//接收数据帧头赋0
			data_tx_high<=8'd0;//发送数据高位赋0
			data_tx_low <=8'd0;//发送数据低位赋0
			data_rx_high<=8'd0;//接收数据高位赋0
			data_rx_low <=8'd0;//接收数据低位赋0
			data_tx_end<=8'd0;//接收数据帧尾赋0
		end
	else 
      if(sel_data)//波特率控制
	      case(num)//接收数据位数
		      0:;//起始位
			   1:rx_data_buff[0]<=fpga_rx;//接收数据第0位
			   2:rx_data_buff[1]<=fpga_rx;//接收数据第1位
				3:rx_data_buff[2]<=fpga_rx;//接收数据第2位
	         4:rx_data_buff[3]<=fpga_rx;//接收数据第3位
				5:rx_data_buff[4]<=fpga_rx;//接收数据第4位
            6:rx_data_buff[5]<=fpga_rx;//接收数据第5位
            7:rx_data_buff[6]<=fpga_rx;//接收数据第6位
            8:rx_data_buff[7]<=fpga_rx;//接收数据第7位
				9://停止位
					begin 
					   case(recev_num)//接收字节状态
							0:
							   begin 
									data_tx_start<=rx_data_buff;//接收第一个字节
								end 
							1:
								begin 
									data_tx_high_buff<=rx_data_buff;//接收第二个字节
								end 
							2:
								begin 
									data_tx_low_buff<=rx_data_buff;//接收第三个字节
								end 
							3:
								begin 
									data_rx_high_buff<=rx_data_buff;//接收第四个字节
								end 
							4:
								begin 
									data_rx_low_buff<=rx_data_buff;//接收第五个字节
								end 
							5:
								begin 
									data_tx_end<=rx_data_buff;//接收第六个字节
									if((data_tx_start==8'haa)&&(rx_data_buff==8'h55))
									begin
										data_tx_high<=data_tx_high_buff;
										data_tx_low<=data_tx_low_buff;
										data_rx_high<=data_rx_high_buff; 
										data_rx_low<=data_rx_low_buff;
									end
									else
									begin
										data_tx_high<=8'b0;
										data_tx_low<=8'b0;
										data_rx_high<=8'b0; 
										data_rx_low<=8'b0;
									end
								end 
							default:;
						endcase
					end 
				default:;
			endcase 

//assign wrong_data_flag=((datatx_high<8'h35)&&((datatx_low==8'h10)||(datatx_low==8'h50)||(datatx_low==8'h90)||(datatx_low==8'hd0)))?:1'b0:1'b1;
always @(posedge clk or negedge rst_n)
	begin 
		if(!rst_n)
			rx_data<=8'd0;
		else 
			if(num==9)
				rx_data<=rx_data_buff;
	end 
reg frez_en;
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   frez_en<=0;
	else 
	   if((num==10)&&(recev_num==5))
		   frez_en<=1;//iic启动信号
		else 
		   frez_en<=0;
//
//always @(posedge clk or negedge rst_n)
//   if(!rst_n)
//	   frez_flag<=0;
//	else 
//	   if((num==10)&&(recev_num==3))
//		   frez_flag<=1;//iic启动信号
//		else 
//		   frez_flag<=0;			
always @(posedge clk or negedge rst_n)
	if(!rst_n)
			frez_flag<=0;
	else 
		if(frez_en)
			frez_flag<=1;
		else 
			if(rx_en)
				frez_flag<=0;
			else 
				frez_flag<=frez_flag;
			
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   tx_en<=0;
	else 
	   if(num==9)
		   tx_en<=1;//接收完毕后，启动发送
		else 
		   tx_en<=0;
endmodule 
			