/*-------------------------------------------------------------------------
QuartusII版本号：13.0
fuction：基于FPGA——EP4C6EF17C8的iic与RDA5820通信
discription:本设计主要实现了往RDA5820寄存器中写入数据，5820芯片地址为0010001b,
				b=0表示写，b=1表示0；02H寄存器写入0x0002表示RDA5820系统复位；02H
				写入0xD281表示RDA5820系统上电；05H写入0x88ff表示配置模式和确定最大
				音量；0x40H写入0x0000表示配置为接收模式；0x03H写入0x1A10表示设置
				发送频率为97.4MHZ。频率计算：0x03写入的数用二进制表示为：
				[15:0]0001101000010000,[15:6]表示通道，则chan=104，[3：2]为BAND，
				带宽选择，00表示87.0-108MHZ,[1:0]表示通道间距，00表示100kHZ；因此，
				frez=100*104(kHz)+87.0MHz=97.4MHz
Time：2015/7/16	
---------------------------------------------------------------------------*/
module iic_re_control(clk,rst_n,data_frez_high,data_frez_low,SCLK_RE,SDA_RE,flag_re,iic_en,receive_flag);
input clk;  //系统时钟
input rst_n;//系统复位
input SCLK_RE; //RDA5820串行时钟信号 
input iic_en;
input receive_flag;

input [7:0]data_frez_low;
input [7:0]data_frez_high;

inout SDA_RE;  //RDA5820串行地址/数据信号

output flag_re;

reg [3:0]count;//控制串并转换
reg [31:0]cnt;//控制延时
reg [7:0]state;//状态寄存器
reg sda_buf;//sda缓存器
reg [7:0]data;//数据缓存器


parameter chip_id=8'h22;//芯片地址与读写命令，0x22写命令，0x23读命令
parameter addr_02=8'h02;//寄存器地址
parameter data_high_rst_02=8'hD0;//02H数据高字节
parameter data_low_rst_02=8'h83;//02H数据低字节,02H先写入0xD083,表示复位
parameter data_high_pow_02=8'hD0;//02H数据高字节
parameter data_low_pow_02=8'h81;//02H数据低字节,02H写入0xD081,表示上电
parameter addr_05=8'h05;
parameter data_voice_high=8'h00;
parameter data_voice_low =8'h3f;//05H写入0x88ff,配置模式和确定最大音量
parameter addr_40=8'h40;
parameter data_tr_high=8'h00;
parameter data_tr_low =8'h00;//40H写入0x0000，选择为接收模式
parameter addr_03=8'h03;
//parameter data_frez_high=8'h1A;
//parameter data_frez_low=8'h10;//03H写入0x1A10设置接收频率为97.4MHZ

//parameter data_frez_high=8'h0f;
//parameter data_frez_low=8'h90;//03H写入0x0f90设置接收频率为93.2MHZ

//parameter data_frez_high=8'h0f;
//parameter data_frez_low=8'h50;//03H写入0x0f50设置接收频率为93.1MHZ

//parameter data_frez_high=8'h0f;
//parameter data_frez_low=8'h10;//03H写入0x0f10设置接收频率为93.0MHZ

//parameter data_frez_high=8'h0f;
//parameter data_frez_low=8'hd0;//03H写入0x0fd0设置接收频率为93.3MHZ

//parameter data_frez_high=8'h1B;
//parameter data_frez_low=8'h90;//03H写入0x0f50设置接收频率为98MHZ
//
//parameter data_frez_high=8'h01;
//parameter data_frez_low=8'hd0;//03H写入0x01d0设置接收频率为87.7MHZ

//parameter data_frez_high=8'h02;
//parameter data_frez_low=8'h10;//03H写入0x01d0设置接收频率为87.7MHZ

assign SDA_RE=(state==40)?1'd1:sda_buf; 
assign flag_re=(state==40)?1'd0:1'd1;

parameter delay_cnt=32'd10000;
parameter delay_cnt1=32'd100000;
parameter delay_powerup=32'd1500000;
reg [31:0]delay_cnt_powerup;
always @(posedge clk or negedge rst_n)
	if(!rst_n)
		delay_cnt_powerup<=1'd0;
	else
		if(delay_cnt_powerup<delay_powerup)
			delay_cnt_powerup<=delay_cnt_powerup+1'd1;
		else
			delay_cnt_powerup<=delay_powerup;

//-----------------	rda5820 control-----------
always @(negedge clk or negedge rst_n) 
	begin
		if(!rst_n)     //所有寄存器复位
			begin
				state<=0;
				sda_buf<=1;
				count<=0;
				data<=0;
				cnt<=0;
			end
		else
			begin
				case(state)
				/*--------------状态0—6表示给02H寄存器中写入0x0002,RDA5820系统复位--------------*/
//--------------send start singial-------------
					0:
						begin
							if((SCLK_RE))
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=1;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=0;
								end 
						end
//--------------send countral word--------------
					1:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_02;//02h地址准备
										state<=2;
									end
								else
									begin 
										state<=1;
									end 
						end
//--------------send  Byte address--------------
					2:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_high_rst_02;//02h高字节数据准备
										state<=3;
									end
								else
									begin 
										state<=2;
									end 
						end
//--------------send high Byte data--------------
					3:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_low_rst_02;//02h高字节数据准备
										state<=4;
									end
								else
									begin 
										state<=3;
									end 
						end
//--------------send low Byte data--------------
					4:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=5;
									end
								else
									begin 
										state<=4;
									end 
						end
//--------------send stop singial--------------
					5:
						begin
							if(!SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=6;
								end 
							else 
								begin 
									state<=5;
								end 
						end
					6:
					   begin
							if(SCLK_RE)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=7;
								end 
							else 
								begin 
									state<=6;
								end 
						end
					7:					 //延时500ms
					   begin
						   if(cnt<delay_cnt1)
							   begin 
									cnt<=cnt+1'd1;
									state<=7;
								end  
							else 
								begin 
								   cnt<=0;
									state<=8;
								end 
						end
						/*--------------状态8—14表示给02H寄存器中写入0xD281,RDA582系统上电--------------*/
//--------------send start singial-------------
					8:
						begin
							if(SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=9;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=8;
								end 
						end
//--------------send countral word--------------
					9:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_02;//02h地址准备
										state<=10;
									end
								else
									begin 
										state<=9;
									end 
						end
//--------------send  Byte address--------------
					10:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_high_pow_02;//02h高字节数据准备
										state<=11;
									end
								else
									begin 
										state<=10;
									end 
						end
//--------------send high Byte data--------------
					11:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_low_pow_02;//02h低字节数据准备
										state<=12;
									end
								else
									begin 
										state<=11;
									end 
						end
//--------------send low Byte data--------------
					12:
						begin
						if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，在0x02写入0xD281,表示上电
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=13;
									end
								else
									begin 
										state<=12;
									end 
						end
//--------------send stop singial--------------
					13:
						begin
							if(!SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=14;
								end 
							else 
								begin 
									state<=13;
								end 
						end
					14:
					   begin
							if(SCLK_RE)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=15;
								end 
							else 
								begin 
									state<=14;
								end 
						end
					15:					 //延时500ms
					   begin
						   if(cnt<delay_cnt1)
							   begin 
									cnt<=cnt+1'd1;
									state<=15;
								end  
							else 
								begin 
								   cnt<=0;
									state<=16;
								end 
						end
						/*--------------状态16—22表示给05H寄存器中写入0x88ff,配置RDA582系统模式以及最大音量
													parameter addr_05=8'h05;
													parameter data_voice_high=8'h88;
													parameter data_voice_low =8'hff;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					16:
						begin
							if(SCLK_RE) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=17;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=16;
								end 
						end
//--------------send countral word--------------
					17:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_05;//02h地址准备
										state<=18;
									end
								else
									begin 
										state<=17;
									end 
						end
//--------------send  Byte address--------------
					18:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_voice_high;//02h高字节数据准备
										state<=19;
									end
								else
									begin 
										state<=18;
									end 
						end
//--------------send high Byte data--------------
					19:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_voice_low;//02h低字节数据准备
										state<=20;
									end
								else
									begin 
										state<=19;
									end 
						end
//--------------send low Byte data--------------
					20:
						begin
						if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=21;
									end
								else
									begin 
										state<=20;
									end 
						end
//--------------send stop singial--------------
					21:
						begin
							if(!SCLK_RE) 
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=22;
								end 
							else 
								begin 
									state<=21;
								end 
						end
					22:
					   begin
							if(SCLK_RE)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=24;
								end 
							else 
								begin 
									state<=22;
								end 
						end
				/*	23:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=23;
								end  
							else 
								begin 
								   cnt<=0;
									state<=24;
								end 
						end*/
						/*--------------状态24—30表示给40H寄存器中写入0x0001,配置RDA582系统为发送模式
													parameter addr_40=8'h40;
													parameter data_tr_high=8'h00;
													parameter data_tr_low =8'h01;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					24:
						begin
							if(SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=25;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=24;
								end 
						end
//--------------send countral word--------------
					25:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_40;
										state<=26;
									end
								else
									begin 
										state<=25;
									end 
						end
//--------------send  Byte address--------------
					26:
						begin
							if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_tr_high;
										state<=27;
									end
								else
									begin 
										state<=26;
									end 
						end
//--------------send high Byte data--------------
					27:
						begin
							if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_tr_low;
										state<=28;
									end
								else
									begin 
										state<=27;
									end 
						end
//--------------send low Byte data--------------
					28:
						begin
						if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=29;
									end
								else
									begin 
										state<=28;
									end 
						end
//--------------send stop singial--------------
					29:
						begin
							if(!SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=30;
								end 
							else 
								begin 
									state<=29;
								end 
						end
					30:
					   begin
							if(SCLK_RE) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=32;
								end 
							else 
								begin 
									state<=30;
								end 
						end
			/*		31:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=31;
								end  
							else 
								begin 
								   cnt<=0;
									state<=32;
								end 
						end*/
						/*--------------状态32—38表示给03H寄存器中写入0x1A10,设置发送频率为97.4MHZ
													parameter addr_03=8'h03;
													parameter data_frez_high=8'h1A;
													parameter data_frez_low=8'h10;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					32:
						begin
							if(SCLK_RE) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=33;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=32;
								end 
						end
//--------------send countral word--------------
					33:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_03;
										state<=34;
									end
								else
									begin 
										state<=33;
									end 
						end
//--------------send  Byte address--------------
					34:
						begin
							if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_frez_high;
										state<=35;
									end
								else
									begin 
										state<=34;
									end 
						end
//--------------send high Byte data--------------
					35:
						begin
							if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_frez_low;
										state<=36;
									end
								else
									begin 
										state<=35;
									end 
						end
//--------------send low Byte data--------------
					36:
						begin
						if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=37;
									end
								else
									begin 
										state<=36;
									end 
						end
//--------------send stop singial--------------
					37:
						begin
							if(!SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=38;
								end 
							else 
								begin 
									state<=37;
								end 
						end
					38:
					   begin
							if(SCLK_RE) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=39;
								end 
							else 
								begin 
									state<=38;
								end 
						end
					39:
						state<=40;
			/*		39:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=39;
								end  
							else 
								begin 
								   cnt<=0;
									state<=40;
								end 
						end  */
					40:
				      begin 
							if(receive_flag)
								state<=40;
							else
								if(iic_en)
									state<=43;
								else
									state<=40;
				      end 
					43:					 //延时50ms
					   begin
						   if(cnt<delay_cnt1)
							   begin 
									cnt<=cnt+1'd1;
									state<=43;
								end  
							else 
								begin 
								   cnt<=0;
									state<=52;
								end 
						end  
//					44:
//						begin
//							if((SCLK_RE))
//								begin		
//									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
//									state<=45;
//									data<=chip_id;//写控制字准备
//								end 
//							else 
//								begin 
//									state<=44;
//								end 
//						end
////--------------send countral word--------------
//					45:
//						begin
//							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
//								begin
//									count<=count+1'b1;
//									data<={data[6:0],data[7]};
//									sda_buf<=data[7];
//								end
//							else 
//								if((count==8)&&(!SCLK_RE))
//									begin
//										count<=0;
//										sda_buf<=0;//send ACK signal
//										data<=addr_02;//02h地址准备
//										state<=46;
//									end
//								else
//									begin 
//										state<=45;
//									end 
//						end
////--------------send  Byte address--------------
//					46:
//						begin
//							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
//								begin
//									count<=count+1'b1;
//									data<={data[6:0],data[7]};
//									sda_buf<=data[7];
//								end
//							else 
//								if((count==8)&&(!SCLK_RE))
//									begin
//										count<=0;
//										sda_buf<=0;//send ACK signal
//										data<=data_high_rst_02;//02h高字节数据准备
//										state<=47;
//									end
//								else
//									begin 
//										state<=46;
//									end 
//						end
////--------------send high Byte data--------------
//					47:
//						begin
//							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
//								begin
//									count<=count+1'b1;
//									data<={data[6:0],data[7]};
//									sda_buf<=data[7];
//								end
//							else 
//								if((count==8)&&(!SCLK_RE))
//									begin
//										count<=0;
//										sda_buf<=0;//send ACK signal
//										data<=data_low_rst_02;//02h高字节数据准备
//										state<=48;
//									end
//								else
//									begin 
//										state<=47;
//									end 
//						end
////--------------send low Byte data--------------
//					48:
//						begin
//							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
//								begin
//									count<=count+1'b1;
//									data<={data[6:0],data[7]};
//									sda_buf<=data[7];
//								end
//							else 
//								if((count==8)&&(!SCLK_RE))
//									begin
//										sda_buf<=1;//send NOACK signal
//										count<=0;
//										state<=49;
//									end
//								else
//									begin 
//										state<=48;
//									end 
//						end
////--------------send stop singial--------------
//					49:
//						begin
//							if(!SCLK_RE)
//								begin		
//									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
//									state<=50;
//								end 
//							else 
//								begin 
//									state<=49;
//								end 
//						end
//					50:
//					   begin
//							if(SCLK_RE)
//								begin		
//									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
//									state<=51;
//								end 
//							else 
//								begin 
//									state<=50;
//								end 
//						end
//					51:					 //延时50ms
//					   begin
//						   if(cnt<delay_cnt)
//							   begin 
//									cnt<=cnt+1'd1;
//									state<=51;
//								end  
//							else 
//								begin 
//								   cnt<=0;
//									state<=52;
//								end 
//						end
						/*--------------状态8—14表示给02H寄存器中写入0xD281,RDA582系统上电--------------*/
//--------------send start singial-------------
					52:
						begin
							if(SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=53;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=52;
								end 
						end
//--------------send countral word--------------
					53:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_02;//02h地址准备
										state<=54;
									end
								else
									begin 
										state<=53;
									end 
						end
//--------------send  Byte address--------------
					54:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_high_pow_02;//02h高字节数据准备
										state<=55;
									end
								else
									begin 
										state<=54;
									end 
						end
//--------------send high Byte data--------------
					55:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_low_pow_02;//02h低字节数据准备
										state<=56;
									end
								else
									begin 
										state<=55;
									end 
						end
//--------------send low Byte data--------------
					56:
						begin
						if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，在0x02写入0xD281,表示上电
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=57;
									end
								else
									begin 
										state<=56;
									end 
						end
//--------------send stop singial--------------
					57:
						begin
							if(!SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=58;
								end 
							else 
								begin 
									state<=57;
								end 
						end
					58:
					   begin
							if(SCLK_RE)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=59;
								end 
							else 
								begin 
									state<=58;
								end 
						end
					59:					 //延时50ms
					   begin
						   if(cnt<delay_cnt1)
							   begin 
									cnt<=cnt+1'd1;
									state<=59;
								end  
							else 
								begin 
								   cnt<=0;
									state<=60;
								end 
						end
						/*--------------状态16—22表示给05H寄存器中写入0x88ff,配置RDA582系统模式以及最大音量
													parameter addr_05=8'h05;
													parameter data_voice_high=8'h88;
													parameter data_voice_low =8'hff;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					60:
						begin
							if(SCLK_RE) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=61;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=60;
								end 
						end
//--------------send countral word--------------
					61:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_05;//02h地址准备
										state<=62;
									end
								else
									begin 
										state<=61;
									end 
						end
//--------------send  Byte address--------------
					62:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_voice_high;//02h高字节数据准备
										state<=63;
									end
								else
									begin 
										state<=62;
									end 
						end
//--------------send high Byte data--------------
					63:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_voice_low;//02h低字节数据准备
										state<=64;
									end
								else
									begin 
										state<=63;
									end 
						end
//--------------send low Byte data--------------
					64:
						begin
						if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=65;
									end
								else
									begin 
										state<=64;
									end 
						end
//--------------send stop singial--------------
					65:
						begin
							if(!SCLK_RE) 
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=66;
								end 
							else 
								begin 
									state<=65;
								end 
						end
					66:
					   begin
							if(SCLK_RE)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=68;
								end 
							else 
								begin 
									state<=66;
								end 
						end
				/*	67:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=67;
								end  
							else 
								begin 
								   cnt<=0;
									state<=68;
								end 
						end*/
						/*--------------状态24—30表示给40H寄存器中写入0x0001,配置RDA582系统为发送模式
													parameter addr_40=8'h40;
													parameter data_tr_high=8'h00;
													parameter data_tr_low =8'h01;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					68:
						begin
							if(SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=69;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=68;
								end 
						end
//--------------send countral word--------------
					69:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_40;
										state<=70;
									end
								else
									begin 
										state<=69;
									end 
						end
//--------------send  Byte address--------------
					70:
						begin
							if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_tr_high;
										state<=71;
									end
								else
									begin 
										state<=70;
									end 
						end
//--------------send high Byte data--------------
					71:
						begin
							if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_tr_low;
										state<=72;
									end
								else
									begin 
										state<=71;
									end 
						end
//--------------send low Byte data--------------
					72:
						begin
						if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=73;
									end
								else
									begin 
										state<=72;
									end 
						end
//--------------send stop singial--------------
					73:
						begin
							if(!SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=74;
								end 
							else 
								begin 
									state<=73;
								end 
						end
					74:
					   begin
							if(SCLK_RE) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=76;
								end 
							else 
								begin 
									state<=74;
								end 
						end
			/*		75:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=75;
								end  
							else 
								begin 
								   cnt<=0;
									state<=76;
								end 
						end*/
						/*--------------状态32—38表示给03H寄存器中写入0x1A10,设置发送频率为97.4MHZ
													parameter addr_03=8'h03;
													parameter data_frez_high=8'h1A;
													parameter data_frez_low=8'h10;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					76:
						begin
							if(SCLK_RE) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=77;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=76;
								end 
						end
//--------------send countral word--------------
					77:
						begin
							if((count<8)&&(!SCLK_RE))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_03;
										state<=78;
									end
								else
									begin 
										state<=77;
									end 
						end
//--------------send  Byte address--------------
					78:
						begin
							if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_frez_high;
										state<=79;
									end
								else
									begin 
										state<=78;
									end 
						end
//--------------send high Byte data--------------
					79:
						begin
							if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_frez_low;
										state<=80;
									end
								else
									begin 
										state<=79;
									end 
						end
//--------------send low Byte data--------------
					80:
						begin
						if((count<8)&&(!SCLK_RE))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_RE))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=81;
									end
								else
									begin 
										state<=80;
									end 
						end
//--------------send stop singial--------------
					81:
						begin
							if(!SCLK_RE)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=82;
								end 
							else 
								begin 
									state<=81;
								end 
						end
					82:
					   begin
							if(SCLK_RE) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=40;
								end 
							else 
								begin 
									state<=82;
								end 
						end
				default:;
				endcase
			end
	end

endmodule 