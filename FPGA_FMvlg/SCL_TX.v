module SCL_TX(clk,rst_n,flag_tx,SCLK_TX);
input clk;
input rst_n;
input flag_tx;

output reg SCLK_TX;

reg count;

reg [31:0]delay_cnt;

parameter delay=32'd1499999;
//parameter delay=32'd10;

always @(posedge clk or negedge rst_n)
	if(!rst_n)
		delay_cnt<=1'd0;
	else
		if(delay_cnt<delay)
			delay_cnt<=delay_cnt+1'd1;
		else
			delay_cnt<=delay;
			
wire state_flag;

assign state_flag=((delay_cnt==delay)&&(flag_tx))?1'd1:1'd0;

		
always@(posedge clk or negedge rst_n)
begin 
        if(!rst_n)
		       begin 
		       SCLK_TX<=1;
		       count<=0;
			    end 
		  else 
		      case(state_flag)
					0:SCLK_TX<=1;
				   1:
						if(count==1) 
							  SCLK_TX=~SCLK_TX; 
						else
							  count<=count+1'd1;
				   default:;
			   endcase 
end
endmodule 
