module uart_tx(clk,rst_n,num,sel_data,rx_data,fpga_tx);
input clk;
input rst_n;
input [4:0]num;//帧数据控制位
input sel_data;//波特率计数中心点
input [7:0]rx_data;
output reg fpga_tx;//输出串行数据

always @(posedge clk or negedge rst_n)
	if(!rst_n)
	   fpga_tx<=1'b1;
	else 
	   if(sel_data)
		   case(num)
			   0:fpga_tx<=1'b0;
				1:fpga_tx<=rx_data[0];
				2:fpga_tx<=rx_data[1];
				3:fpga_tx<=rx_data[2];
				4:fpga_tx<=rx_data[3];
				5:fpga_tx<=rx_data[4];
				6:fpga_tx<=rx_data[5];
				7:fpga_tx<=rx_data[6];
				8:fpga_tx<=rx_data[7];
				9:fpga_tx<=1'b1;
				10:fpga_tx<=1'b0;
				default:fpga_tx<=1'b1;
			endcase
endmodule 
