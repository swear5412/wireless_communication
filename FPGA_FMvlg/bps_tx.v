module bps_tx(clk,rst_n,en,sel_data1,num1);
input clk;
input rst_n;
input en;

output reg sel_data1;
output reg[4:0]num1;

parameter bps_div=13'd5207;
parameter bsp_div_2=13'd2603;

reg flag;

always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   flag<=0;
	else 
	   if(en)
		   flag<=1;
		else 
		   if(num1==5'd10)//当接收/发送完一帧数据后将flag标志位拉低
			   flag<=0;
			else 
			   flag<=flag;
				
reg [12:0]cnt;
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   cnt<=13'd0;
	else 
	   if(flag&&cnt<bps_div)
		   cnt<=cnt+1'b1;
		else 
		   cnt<=13'd0;
			
always @(posedge clk or negedge rst_n)
	if(!rst_n)
	   num1<=5'd0;
	else 
	   if(sel_data1&&flag)
		   num1<=num1+1'b1;
		else 
		   if(num1==5'd10)
			   num1<=1'd0;
				
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   sel_data1<=1'b0;
	else 
	   if(cnt==bsp_div_2)
		   sel_data1<=1'b1;
		else
		   sel_data1<=1'b0;
endmodule 
		
				