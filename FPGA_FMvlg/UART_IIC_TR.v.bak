module UART_IIC_TR(clk,rst_n,fpga_rx,fpga_tx,SDA_RE,SDA_TX,SCLK_RE,SCLK_TX);
input clk;//系统时钟
input rst_n;//系统复位

inout SDA_RE;//RDA5820接收控制总线
inout SDA_TX;//RDA5820发送控制总线

output SCLK_RE;//RDA5820接收时钟控制总线
output SCLK_TX;//RDA5820发送时钟控制总线

input fpga_rx;//串口接收
output fpga_tx;//串口发送

wire freq_out_tx;//发送模块时钟控制信号，为SCLK_RE频率的2倍
wire freq_out_re;//接收模块时钟控制信号，为SCLK_TX频率的2倍
wire flag_tx;//SCLK_TX时钟控制信号，iic工作一次后，时钟停止工作
wire flag_re;//SCLK_RE时钟控制信号
wire uart_en;//启动串口信号，iic工作一次后，启动串口信号
wire iic_en;//串口发送数据后，iic启动控制信号

wire [7:0]data_tx_high;
wire [7:0]data_tx_low;
wire [7:0]data_rx_high;
wire [7:0]data_rx_low;
wire [7:0]tx_highbyte;
wire [7:0]tx_lowbyte;
wire [7:0]rx_highbyte;
wire [7:0]rx_lowbyte;

wire [7:0]rx_data;

wire tx_en;
wire sel_data1;
wire [4:0]num1;
wire rx_en;
wire sel_data;
wire [4:0]num;
wire [1:0]recev_num;
wire frez_flag;//工作频率控制信号，0：表示使用默认频率，1：使用串口发送来的频率
wire receive_flag;

bps_tx bps_tx(
						.clk(clk),
						.rst_n(rst_n),
						.en(tx_en),
						.sel_data1(sel_data1),
						.num1(num1)
						);
bps_rx bps_rx(
						.clk(clk),
						.rst_n(rst_n),
						.rx_en(rx_en),
						.sel_data(sel_data),
						.num(num),
						.recev_num(recev_num),
						.receive_flag(receive_flag)
						);
uart_tx uart_tx(
							.clk(clk),
							.rst_n(rst_n),
							.num(num1),
							.sel_data(sel_data1),
							.rx_data(rx_data),
							.fpga_tx(fpga_tx)
							);
uart_rx uart_rx(
							.clk(clk),
							.rst_n(rst_n),
							.fpga_rx(fpga_rx),
							.num(num),
							.recev_num(recev_num),
							.sel_data(sel_data),
							.uart_en(uart_en),
							.rx_en(rx_en),
							.tx_en(tx_en),
							.frez_flag(frez_flag),
							.data_tx_high(data_tx_high),
							.data_tx_low(data_tx_low),
							.data_rx_high(data_rx_high),
							.data_rx_low(data_rx_low),
							.rx_data(rx_data)
							);
freq_400k_tx freq_400k_tx(
										.clk(clk),
										.rst_n(rst_n),
										.freq_out_tx(freq_out_tx)
										);
freq_400k_re freq_400k_re(
										.clk(clk),
										.rst_n(rst_n),
										.freq_out_re(freq_out_re)
										);
SCL_TX SCL_TX(
						.clk(freq_out_tx),
						.rst_n(rst_n),
						.flag_tx(flag_tx),
						.SCLK_TX(SCLK_TX)
						);
SCL_RE SCL_RE(
						.clk(freq_out_re),
						.rst_n(rst_n),
						.flag_re(flag_re),
						.SCLK_RE(SCLK_RE)
						);
frez_control frez_control( 
										.clk(clk),
										.rst_n(rst_n),
										.frez_flag(frez_flag),
										.datatx_high(data_tx_high),
										.datatx_low(data_tx_low),
										.datarx_high(data_rx_high),
										.datarx_low(data_rx_low),
										.tx_highbyte(tx_highbyte),
										.tx_lowbyte(tx_lowbyte),
										.rx_highbyte(rx_highbyte),
										.rx_lowbyte(rx_lowbyte)
								);
iic_tx_control iic_tx_control(
											.clk(freq_out_tx),
											.rst_n(rst_n),
											.data_frez_high(tx_highbyte),
											.data_frez_low(tx_lowbyte),
											.SCLK_TX(SCLK_TX),
											.SDA_TX(SDA_TX),
											.uart_en(uart_en),
											.flag_tx(flag_tx),
											.frez_flag(frez_flag),
											.iic_en(iic_en),
											.receive_flag(receive_flag)
											);
iic_re_control iic_re_control(
											.clk(freq_out_re),
											.rst_n(rst_n),
											.data_frez_high(rx_highbyte),
											.data_frez_low(rx_lowbyte),
											.SCLK_RE(SCLK_RE),
											.SDA_RE(SDA_RE),
											.flag_re(flag_re),
											.iic_en(iic_en),
											.receive_flag(receive_flag)
											);								

endmodule 