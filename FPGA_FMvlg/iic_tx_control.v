/*-------------------------------------------------------------------------
QuartusII版本号：13.0
fuction：基于FPGA——EP4C6EF17C8的iic与RDA5820通信
discription:本设计主要实现了往RDA5820寄存器中写入数据，5820芯片地址为0010001b,
				b=0表示写，b=1表示0；02H寄存器写入0x0002表示RDA5820系统复位；02H
				写入0xD281表示RDA5820系统上电；05H写入0x88ff表示配置模式和确定最大
				音量；0x40H写入0x0001表示配置为发送模式；0x03H写入0x1A10表示设置
				发送频率为97.4MHZ。频率计算：0x03写入的数用二进制表示为：
				[15:0]0001101000010000,[15:6]表示通道，则chan=104，[3：2]为BAND，
				带宽选择，00表示87.0-108MHZ,[1:0]表示通道间距，00表示100kHZ；因此，
				frez=100*104(kHz)+87.0MHz=97.4MHz
Time：2015/7/16	
---------------------------------------------------------------------------*/
module iic_tx_control(clk,rst_n,data_frez_high,data_frez_low,SCLK_TX,SDA_TX,flag_tx,uart_en,frez_flag,receive_flag,
						iic_en);
input clk;  //系统时钟
input rst_n;//系统复位
input SCLK_TX; //RDA5820串行时钟信号
input frez_flag;
input [7:0]data_frez_low;
input [7:0]data_frez_high;
input receive_flag;


inout SDA_TX;  //RDA5820串行地址/数据信号

output flag_tx;
output reg uart_en;

reg [3:0]count;//控制串并转换
reg [31:0]cnt;//控制延时
reg [7:0]state;//状态寄存器
reg sda_buf;//sda缓存器
reg [7:0]data;//数据缓存器

parameter chip_id=8'h58;//芯片地址与读写命令，0x22写命令，0x23读命令
parameter addr_00=8'h00;//寄存器地址
parameter data_rst_00=8'h81;//复位
parameter data_cal_00=8'h41;//有限状态机校验
parameter data_calclp_00 =8'h01;//有限状态机校验完成
parameter addr_03=8'h03;
parameter data_extc_03=8'h10;//外部时钟输入
parameter addr_04=8'h04;
parameter data_chsclk_04=8'h33;//选择12MHZ时钟
parameter addr_18=8'h18;
parameter data_snr_18=8'hE4;//改善信噪比
parameter addr_1B=8'h1B;
parameter data_rfmax_1B=8'hF0;//发射功率最大
parameter addr_02=8'h02;
parameter data_pacls_02=8'hb9;//一分钟没有音频输入时放弃RF PA功能
parameter addr_01=8'h01;//频率设置

parameter delay_cnt=32'd20000;
parameter delay_powerup=32'd1500000;

output iic_en;


assign SDA_TX=(state==75)?1'd1:sda_buf; 
assign flag_tx=(state==75)?1'd0:1'd1;

reg in_a;
reg in_b;
always @(posedge clk or negedge rst_n)
   if(!rst_n)
	   begin
		   in_a<=1'b0;
			in_b<=1'b0;
		end 
	else 
	   begin 
		   in_a<=frez_flag;
			in_b<=in_a;
		end
		
assign iic_en=in_a&(~in_b);

reg [31:0]delay_cnt_powerup;
always @(posedge clk or negedge rst_n)
	if(!rst_n)
		delay_cnt_powerup<=1'd0;
	else
		if(delay_cnt_powerup<delay_powerup)
			delay_cnt_powerup<=delay_cnt_powerup+1'd1;
		else
			delay_cnt_powerup<=delay_powerup;
//-----------------	rda5820 control-----------
always @(negedge clk or negedge rst_n) 
	begin
		if(!rst_n)     //所有寄存器复位
			begin
				state<=0;
				sda_buf<=1;
				count<=0;
				data<=0;
				cnt<=0;
			end
		else
			if(delay_cnt_powerup<delay_powerup)
				sda_buf<=1;
			else
			begin
				case(state)
				/*--------------状态0—6表示给02H寄存器中写入0x0002,RDA5820系统复位--------------*/
//--------------send start singial-------------
					0:
						begin
							if((SCLK_TX))
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=1;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=0;
								end 
						end
//--------------send countral word--------------
					1:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_00;//00h地址准备
										state<=2;
									end
								else
									begin 
										state<=1;
									end 
						end
//--------------send  Byte address--------------
					2:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_rst_00;//02h高字节数据准备
										state<=4;
									end
								else
									begin 
										state<=2;
									end 
						end

//--------------send low Byte data--------------
					4:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=5;
									end
								else
									begin 
										state<=4;
									end 
						end
//--------------send stop singial--------------
					5:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=6;
								end 
							else 
								begin 
									state<=5;
								end 
						end
					6:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=7;
								end 
							else 
								begin 
									state<=6;
								end 
						end
					7:					 //延时100ms
					   begin
						   if(cnt<delay_cnt)
							   begin 
									cnt<=cnt+1'd1;
									state<=7;
								end  
							else 
								begin 
								   cnt<=0;
									state<=8;
								end 
						end
						/*--------------状态8—14表示给02H寄存器中写入0xD281,RDA582系统上电--------------*/
//--------------send start singial-------------
					8:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=9;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=8;
								end 
						end
//--------------send countral word--------------
					9:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_03;//02h地址准备
										state<=10;
									end
								else
									begin 
										state<=9;
									end 
						end
//--------------send  Byte address--------------
					10:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_extc_03;//02h高字节数据准备
										state<=12;
									end
								else
									begin 
										state<=10;
									end 
						end

//--------------send low Byte data--------------
					12:
						begin
						if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，在0x02写入0xD281,表示上电
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=13;
									end
								else
									begin 
										state<=12;
									end 
						end
//--------------send stop singial--------------
					13:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=14;
								end 
							else 
								begin 
									state<=13;
								end 
						end
					14:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=15;
								end 
							else 
								begin 
									state<=14;
								end 
						end
					15:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=15;
								end  
							else 
								begin 
								   cnt<=0;
									state<=16;
								end 
						end
						/*--------------状态16—22表示给05H寄存器中写入0x88ff,配置RDA582系统模式以及最大音量
													parameter addr_05=8'h05;
													parameter data_voice_high=8'h88;
													parameter data_voice_low =8'hff;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					16:
						begin
							if(SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=17;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=16;
								end 
						end
//--------------send countral word--------------
					17:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_04;//02h地址准备
										state<=18;
									end
								else
									begin 
										state<=17;
									end 
						end
//--------------send  Byte address--------------
					18:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_chsclk_04;//02h高字节数据准备
										state<=20;
									end
								else
									begin 
										state<=18;
									end 
						end

//--------------send low Byte data--------------
					20:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=21;
									end
								else
									begin 
										state<=20;
									end 
						end
////--------------send stop singial--------------
					21:
						begin
							if(!SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=22;
								end 
							else 
								begin 
									state<=21;
								end 
						end
					22:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=23;
								end 
							else 
								begin 
									state<=22;
								end 
						end
				23:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=23;
								end  
							else 
								begin 
								   cnt<=0;
									state<=24;
								end 
						end
						/*--------------状态24—30表示给40H寄存器中写入0x0001,配置RDA582系统为发送模式
													parameter addr_40=8'h40;
													parameter data_tr_high=8'h00;
													parameter data_tr_low =8'h01;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					24:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=25;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=24;
								end 
						end
//--------------send countral word--------------
					25:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_00;
										state<=26;
									end
								else
									begin 
										state<=25;
									end 
						end
//--------------send  Byte address--------------
					26:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_cal_00;
										state<=28;
									end
								else
									begin 
										state<=26;
									end 
						end
//--------------send low Byte data--------------
					28:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=29;
									end
								else
									begin 
										state<=28;
									end 
						end
//--------------send stop singial--------------
					29:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=30;
								end 
							else 
								begin 
									state<=29;
								end 
						end
					30:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=31;
								end 
							else 
								begin 
									state<=30;
								end 
						end
					31:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=31;
								end  
							else 
								begin 
								   cnt<=0;
									state<=32;
								end 
						end
						/*--------------状态32—38表示给03H寄存器中写入0x1A10,设置发送频率为97.4MHZ
													parameter addr_03=8'h03;
													parameter data_frez_high=8'h1A;
													parameter data_frez_low=8'h10;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					32:
						begin
							if(SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=33;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=32;
								end 
						end
//--------------send countral word--------------
					33:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_00;
										state<=34;
									end
								else
									begin 
										state<=33;
									end 
						end
//--------------send  Byte address--------------
					34:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_calclp_00;
										state<=35;
									end
								else
									begin 
										state<=34;
									end 
						end

//--------------send low Byte data--------------
					35:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=36;
									end
								else
									begin 
										state<=35;
									end 
						end
//--------------send stop singial--------------
					36:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=37;
								end 
							else 
								begin 
									state<=36;
								end 
						end
					37:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=38;
								end 
							else 
								begin 
									state<=37;
								end 
						end
				38:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=38;
								end  
							else 
								begin 
								   cnt<=0;
									state<=39;
								end 
						end	
				 39:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=40;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=39;
								end 
						end
//--------------send countral word--------------
					40:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_18;
										state<=41;
									end
								else
									begin 
										state<=40;
									end 
						end
//--------------send  Byte address--------------
					41:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_snr_18;
										state<=42;
									end
								else
									begin 
										state<=41;
									end 
						end

//--------------send low Byte data--------------
					42:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=43;
									end
								else
									begin 
										state<=42;
									end 
						end
//--------------send stop singial--------------
					43:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=44;
								end 
							else 
								begin 
									state<=43;
								end 
						end
					44:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=45;
								end 
							else 
								begin 
									state<=44;
								end 
						end
		45:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=45;
								end  
							else 
								begin 
								   cnt<=0;
									state<=46;
								end 
						end					
					46:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=47;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=46;
								end 
						end
//--------------send countral word--------------
					47:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_1B;
										state<=48;
									end
								else
									begin 
										state<=47;
									end 
						end
//--------------send  Byte address--------------
					48:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_rfmax_1B;
										state<=49;
									end
								else
									begin 
										state<=48;
									end 
						end

//--------------send low Byte data--------------
					49:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=50;
									end
								else
									begin 
										state<=49;
									end 
						end
//--------------send stop singial--------------
					50:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=51;
								end 
							else 
								begin 
									state<=50;
								end 
						end
					51:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=52;
								end 
							else 
								begin 
									state<=51;
								end 
						end
							52:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=52;
								end  
							else 
								begin 
								   cnt<=0;
									state<=53;
								end 
						end
					53:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=54;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=53;
								end 
						end
//--------------send countral word--------------
					54:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_01;
										state<=55;
									end
								else
									begin 
										state<=54;
									end 
						end
//--------------send  Byte address--------------
					55:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_frez_low;
										state<=56;
									end
								else
									begin 
										state<=55;
									end 
						end

//--------------send low Byte data--------------
					56:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=57;
									end
								else
									begin 
										state<=56;
									end 
						end
//--------------send stop singial--------------
					57:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=58;
								end 
							else 
								begin 
									state<=57;
								end 
						end
					58:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=59;
								end 
							else 
								begin 
									state<=58;
								end 
						end
							59:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=59;
								end  
							else 
								begin 
								   cnt<=0;
									state<=60;
								end 
						end
					60:
						begin
							if(SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=61;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=60;
								end 
						end
//--------------send countral word--------------
					61:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_02;//02h地址准备
										state<=62;
									end
								else
									begin 
										state<=61;
									end 
						end
//--------------send  Byte address--------------
					62:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_pacls_02;//02h高字节数据准备
										state<=63;
									end
								else
									begin 
										state<=62;
									end 
						end

//--------------send low Byte data--------------
					63:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=64;
									end
								else
									begin 
										state<=63;
									end 
						end
////--------------send stop singial--------------
					64:
						begin
							if(!SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=65;
								end 
							else 
								begin 
									state<=64;
								end 
						end
					65:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=66;
								end 
							else 
								begin 
									state<=65;
								end 
						end
							66:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=66;
								end  
							else 
								begin 
								   cnt<=0;
									state<=67;
								end 
						end
				67:
						begin
							if(SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=68;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=67;
								end 
						end
//--------------send countral word--------------
					68:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_00;//02h地址准备
										state<=69;
									end
								else
									begin 
										state<=68;
									end 
						end
//--------------send  Byte address--------------
					69:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_frez_high;//02h高字节数据准备
										state<=70;
									end
								else
									begin 
										state<=69;
									end 
						end

//--------------send low Byte data--------------
					70:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=71;
									end
								else
									begin 
										state<=70;
									end 
						end
////--------------send stop singial--------------
					71:
						begin
							if(!SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=72;
								end 
							else 
								begin 
									state<=71;
								end 
						end
					72:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=73;
								end 
							else 
								begin 
									state<=72;
								end 
						end
			
					73:
						state<=74;
				74:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=74;
								end  
							else 
								begin 
								   cnt<=0;
									state<=75;
								end 
						end  
					75:
				      begin 
							if(receive_flag)
								state<=75;
							else
								if(iic_en)
									state<=76;
								else
									state<=75;
				      end 
					76:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=76;
								end  
							else 
								begin 
								   cnt<=0;
									state<=77;
								end 
						end  
					77:
						begin
							if((SCLK_TX))
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=78;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=77;
								end 
						end
//--------------send countral word--------------
					78:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_00;//02h地址准备
										state<=79;
									end
								else
									begin 
										state<=78;
									end 
						end
//--------------send  Byte address--------------
					79:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_rst_00;//02h高字节数据准备
										state<=80;
									end
								else
									begin 
										state<=79;
									end 
						end

//--------------send low Byte data--------------
					80:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=81;
									end
								else
									begin 
										state<=80;
									end 
						end
//--------------send stop singial--------------
					81:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=82;
								end 
							else 
								begin 
									state<=81;
								end 
						end
					82:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=83;
								end 
							else 
								begin 
									state<=82;
								end 
						end
					83:					 //延时50ms
					   begin
						   if(cnt<delay_cnt)
							   begin 
									cnt<=cnt+1'd1;
									state<=83;
								end  
							else 
								begin 
								   cnt<=0;
									state<=84;
								end 
						end
						/*--------------状态8—14表示给02H寄存器中写入0xD281,RDA582系统上电--------------*/
//--------------send start singial-------------
					84:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=85;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=84;
								end 
						end
//--------------send countral word--------------
					85:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_03;//02h地址准备
										state<=86;
									end
								else
									begin 
										state<=85;
									end 
						end
//--------------send  Byte address--------------
					86:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_extc_03;//02h高字节数据准备
										state<=87;
									end
								else
									begin 
										state<=86;
									end 
						end

//--------------send low Byte data--------------
					87:
						begin
						if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，在0x02写入0xD281,表示上电
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=88;
									end
								else
									begin 
										state<=87;
									end 
						end
//--------------send stop singial--------------
					88:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=89;
								end 
							else 
								begin 
									state<=88;
								end 
						end
					89:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=90;
								end 
							else 
								begin 
									state<=89;
								end 
						end
					90:					 //延时50ms
					   begin
						   if(cnt<delay_cnt)
							   begin 
									cnt<=cnt+1'd1;
									state<=90;
								end  
							else 
								begin 
								   cnt<=0;
									state<=91;
								end 
						end
						/*--------------状态16—22表示给05H寄存器中写入0x88ff,配置RDA582系统模式以及最大音量
													parameter addr_05=8'h05;
													parameter data_voice_high=8'h88;
													parameter data_voice_low =8'hff;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					91:
						begin
							if(SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=92;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=91;
								end 
						end
//--------------send countral word--------------
					92:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_04;//02h地址准备
										state<=93;
									end
								else
									begin 
										state<=92;
									end 
						end
//--------------send  Byte address--------------
					93:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_chsclk_04;//02h高字节数据准备
										state<=94;
									end
								else
									begin 
										state<=93;
									end 
						end

////--------------send low Byte data--------------
					94:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=95;
									end
								else
									begin 
										state<=94;
									end 
						end
//--------------send stop singial--------------
					95:
						begin
							if(!SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=96;
								end 
							else 
								begin 
									state<=95;
								end 
						end
					96:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=97;
								end 
							else 
								begin 
									state<=96;
								end 
						end
					97:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=97;
								end  
							else 
								begin 
								   cnt<=0;
									state<=98;
								end 
						end
						/*--------------状态24—30表示给40H寄存器中写入0x0001,配置RDA582系统为发送模式
													parameter addr_40=8'h40;
													parameter data_tr_high=8'h00;
													parameter data_tr_low =8'h01;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					98:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=99;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=98;
								end 
						end
//--------------send countral word--------------
					99:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_00;
										state<=100;
									end
								else
									begin 
										state<=99;
									end 
						end
//--------------send  Byte address--------------
					100:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_cal_00;
										state<=101;
									end
								else
									begin 
										state<=100;
									end 
						end

//--------------send low Byte data--------------
					101:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=102;
									end
								else
									begin 
										state<=101;
									end 
						end
//--------------send stop singial--------------
					102:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=103;
								end 
							else 
								begin 
									state<=102;
								end 
						end
					103:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=104;
								end 
							else 
								begin 
									state<=103;
								end 
						end
				104:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=104;
								end  
							else 
								begin 
								   cnt<=0;
									state<=105;
								end 
						end
						/*--------------状态32—38表示给03H寄存器中写入0x1A10,设置发送频率为97.4MHZ
													parameter addr_03=8'h03;
													parameter data_frez_high=8'h1A;
													parameter data_frez_low=8'h10;
						---------------------------------------------------------------------------------*/
//--------------send start singial-------------
					105:
						begin
							if(SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=106;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=105;
								end 
						end
//--------------send countral word--------------
					106:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_00;
										state<=107;
									end
								else
									begin 
										state<=106;
									end 
						end
//--------------send  Byte address--------------
					107:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_calclp_00;
										state<=108;
									end
								else
									begin 
										state<=107;
									end 
						end
//--------------send low Byte data--------------
					108:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=109;
									end
								else
									begin 
										state<=108;
									end 
						end
//--------------send stop singial--------------
					109:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=110;
								end 
							else 
								begin 
									state<=109;
								end 
						end
					110:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=111;
								end 
							else 
								begin 
									state<=110;
								end 
						end
					111:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=111;
								end  
							else 
								begin 
								   cnt<=0;
									state<=112;
								end 
						end
					112:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=113;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=112;
								end 
						end
//--------------send countral word--------------
					113:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_18;
										state<=114;
									end
								else
									begin 
										state<=113;
									end 
						end
//--------------send  Byte address--------------
					114:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_snr_18;
										state<=115;
									end
								else
									begin 
										state<=114;
									end 
						end

//--------------send low Byte data--------------
					115:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=116;
									end
								else
									begin 
										state<=115;
									end 
						end
//--------------send stop singial--------------
					116:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=117;
								end 
							else 
								begin 
									state<=116;
								end 
						end
					117:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=118;
								end 
							else 
								begin 
									state<=117;
								end 
						end
							118:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=118;
								end  
							else 
								begin 
								   cnt<=0;
									state<=119;
								end 
						end
					119:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=120;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=119;
								end 
						end
//--------------send countral word--------------
					120:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_1B;
										state<=121;
									end
								else
									begin 
										state<=120;
									end 
						end
//--------------send  Byte address--------------
					121:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_rfmax_1B;
										state<=122;
									end
								else
									begin 
										state<=121;
									end 
						end

//--------------send low Byte data--------------
					122:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=123;
									end
								else
									begin 
										state<=122;
									end 
						end
//--------------send stop singial--------------
					123:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=124;
								end 
							else 
								begin 
									state<=123;
								end 
						end
					124:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=125;
								end 
							else 
								begin 
									state<=124;
								end 
						end
				125:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=125;
								end  
							else 
								begin 
								   cnt<=0;
									state<=126;
								end 
						end
					126:
						begin
							if(SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=127;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=126;
								end 
						end
//--------------send countral word--------------
					127:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_01;
										state<=128;
									end
								else
									begin 
										state<=127;
									end 
						end
//--------------send  Byte address--------------
					128:
						begin
							if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_frez_low;
										state<=129;
									end
								else
									begin 
										state<=128;
									end 
						end

//--------------send low Byte data--------------
					129:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=130;
									end
								else
									begin 
										state<=129;
									end 
						end
//--------------send stop singial--------------
					130:
						begin
							if(!SCLK_TX)
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=131;
								end 
							else 
								begin 
									state<=130;
								end 
						end
					131:
					   begin
							if(SCLK_TX) 
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=132;
								end 
							else 
								begin 
									state<=131;
								end 
						end
				132:					 //延时50ms
						begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=132;
								end  
							else 
								begin 
								   cnt<=0;
									state<=133;
								end 
						end
					133:
						begin
							if(SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=134;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=133;
								end 
						end
//--------------send countral word--------------
					134:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_02;//02h地址准备
										state<=135;
									end
								else
									begin 
										state<=134;
									end 
						end
//--------------send  Byte address--------------
					135:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_pacls_02;//02h高字节数据准备
										state<=136;
									end
								else
									begin 
										state<=135;
									end 
						end

//--------------send low Byte data--------------
					136:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=137;
									end
								else
									begin 
										state<=136;
									end 
						end
////--------------send stop singial--------------
					137:
						begin
							if(!SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=138;
								end 
							else 
								begin 
									state<=137;
								end 
						end
					138:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=139;
								end 
							else 
								begin 
									state<=138;
								end 
						end
					139:					 //延时50ms
					   begin
						   if(cnt<10000)
							   begin 
									cnt<=cnt+1'd1;
									state<=139;
								end  
							else 
								begin 
								   cnt<=0;
									state<=140;
								end 
						end
					140:
						begin
							if(SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为高电平，SDA从高电平到低电平，表示启动信号
									state<=141;
									data<=chip_id;//写控制字准备
								end 
							else 
								begin 
									state<=140;
								end 
						end
//--------------send countral word--------------
					141:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出芯片ID以及写控制字
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=addr_00;//02h地址准备
										state<=142;
									end
								else
									begin 
										state<=141;
									end 
						end
//--------------send  Byte address--------------
					142:
						begin
							if((count<8)&&(!SCLK_TX))//在scl低电平期间，完成并串转换，发出02h字节地址
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										count<=0;
										sda_buf<=0;//send ACK signal
										data<=data_frez_high;//02h高字节数据准备
										state<=143;
									end
								else
									begin 
										state<=142;
									end 
						end

//--------------send low Byte data--------------
					143:
						begin
						if((count<8)&&(!SCLK_TX))
								begin
									count<=count+1'b1;
									data<={data[6:0],data[7]};
									sda_buf<=data[7];
								end
							else 
								if((count==8)&&(!SCLK_TX))
									begin
										sda_buf<=1;//send NOACK signal
										count<=0;
										state<=144;
									end
								else
									begin 
										state<=143;
									end 
						end
////--------------send stop singial--------------
					144:
						begin
							if(!SCLK_TX) 
								begin		
									sda_buf<=0; //SCLK为低电平拉低SDA，准备进入停止状态
									state<=145;
								end 
							else 
								begin 
									state<=144;
								end 
						end
					145:
					   begin
							if(SCLK_TX)
								begin		
									sda_buf<=1; //SCLK为高电平，SDA从低电平到高电平，表示停止信号
									state<=75;
								end 
							else 
								begin 
									state<=145;
								end 
						end		
				default:;
				endcase
			end
	end
always @(posedge clk or negedge rst_n)
	if(!rst_n)
		uart_en<=0;
	else 
		if(state==75)
			uart_en<=1; 
endmodule 