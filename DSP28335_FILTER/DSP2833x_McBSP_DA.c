#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File

void McBSPA_TLV5636_WriteCommand(unsigned int data)
{
    while( McbspaRegs.SPCR2.bit.XRDY == 0 );
    McbspaRegs.DXR1.all=(data & 0x0f003);
}


//通过McBSPA向DA器件TLV5636的DAC寄存器写DA数据函数
void McBSPA_TLV5636_WriteData(unsigned int data)
{
    while( McbspaRegs.SPCR2.bit.XRDY == 0 );
    McbspaRegs.DXR1.all=(0x4000 | data);
}

//DA器件TLV5636初始化操作函数
void McBSPA_TLV5636_Init(void)
{
    McBSPA_TLV5636_WriteCommand(0xd003);//R1=R0=1（工作于写数据到控制寄存器模式）,SPD=1(工作于快速模式)，PWR=0(工作于非power down模式)，REF1＝1且REF0=1，选择外部3.3V参考电压
    DSP28x_usDelay(10);
}

