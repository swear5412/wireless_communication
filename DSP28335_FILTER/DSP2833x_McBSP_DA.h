
//通过McBSPA向DA器件TLV5636的控制寄存器写命令函数
void McBSPA_TLV5636_WriteCommand(unsigned int data);
//通过McBSP1向DA器件TLV5636的DAC寄存器写DA数据函数
void McBSPA_TLV5636_WriteData(unsigned int data);
//DA器件TLV5636初始化操作函数
void McBSPA_TLV5636_Init(void);
